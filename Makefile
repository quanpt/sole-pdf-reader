UI.PY=ui_mainwindow.py ui_about.py ui_pdfwindow.py

all: $(UI.PY)
	make run
	# echo "'make run' to run"

$(UI.PY): %.py: %.ui
	pyuic4 $< > $@

run:
	python main.py

clean:
	rm -f $(UI.PY)

send: $(UI.PY)
	cxfreeze main.py --target-dir bin --include-modules=scipy,numpy
	#scp bin/main plum:~/assi/solegui/bin/
	#tar czf solegui.bin.tgz bin
	#scp solegui.bin.tgz plum:~/assi/solegui/
